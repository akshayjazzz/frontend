import React from "react";
import MainHead from "../common/mainhead";

export default function Layout(props) {
  return (
    <>
      <MainHead title={props.title} />

      {props.children}
    </>
  );
}
