import React from "react";
import Styled from "styled-components";

const BannerWrapper = Styled.div`
background-image: url(${(props) => props.backImage});
height: ${(props) => (props.height ? props.height : "100vh")};
background-size: contain;
background-repeat: no-repeat
`;
const BannerContent = Styled.p`
color: red;
`;
export default function Banner(props) {
  return (
    <BannerWrapper backImage={props.image}>
      <BannerContent>{props.content}</BannerContent>
    </BannerWrapper>
  );
}
