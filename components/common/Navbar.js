/* eslint-disable @next/next/no-img-element */
import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { Button } from "react-bootstrap";

export default function TopNavbar() {
  return (
    <>
      <Navbar>
        <Container className="nav-container">
          <Navbar.Brand href="#home">LOGO HERE</Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Nav className="rightNav">
              <Nav.Link href="#home">About Us</Nav.Link>
              <Nav.Link href="#features">Packages</Nav.Link>
              <Nav.Link href="#pricing">Trainer Section</Nav.Link>
              <Button className="download-btn">Download</Button>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
