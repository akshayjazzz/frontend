import React from "react";
import Banner from "../components/common/Banner";
import Layout from "../components/common/Layout";
import TopNavbar from "../components/common/Navbar";
import * as Content from "../utils/reusableContent";
export default function Home() {
  return (
    <Layout title="Home">
      <TopNavbar />
      <Banner
        content={Content.HomeBannerHead}
        image="/images/homeBanner.png"
        height={"80vh"}
      />
    </Layout>
  );
}
